Creació de pàgines dinàmiques
=============================

MP9UF1A2EP1

Exercici pràctic (EP1)

Característiques de l’exercici
------------------------------

En aquest exercici no es disposarà d’accés a Internet.

### Tipus d’exercici

Aquest exercici servirà per mesurar les habilitats en la programació de
simples scripts CGI amb plantilles.

### Enunciat

El dia de realització de l’exercici el seu enunciat estarà disponible en
format XHTML en el [repositori local](_EP1.html) de fitxers.

### Criteri de qualificació

L’exercici aporta el 50% de la nota del resultat d’aprenentatge associat
a l’activitat.

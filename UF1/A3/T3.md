Dissenyar la interfície d’usuari
================================

MP9UF1A3T3

Disseny de formularis amb HTML

Llibre de visites
-----------------

Ara que disposem d’un mòdul per gestionar la base de dades de llibre de
visites ja podem implementar la pàgina web que l’ha de mostrar. Aquesta
pàgina farà servir una plantilla de Genshi i un script CGI. En crear
aquests fitxers ha de tenir en compte aquests aspectes:

-   L’adreça (URL) de la pàgina a visitar ha de ser la del script.
-   Els visitants no poden saber que plantilla i script existeixen en
    fitxers separats.
-   En la pàgina a generar mostra primer les entrades del llibre de
    visites i al final un formulari per inserir noves entrades.
-   Tots els camps del formulari han de ser obligatoris.
-   Valida el contingut dels camps, per exemple verificant que l’adreça
    de correu te el format apropiat. Per fer això et pot servir una
    expressió regular com la que mostra el següent exemple:

        import re

        EMAIL_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")

        def valid_email(email):
            return bool(EMAIL_RE.match(email))
                        

Enllaços recomanats
-------------------

-   En la documentació local de
    [Genshi](file:///usr/share/doc/python-genshi-0.7/doc/xml-templates.html):
    *Genshi XML Template Language*.

Pràctiques
----------

**Atenció**: et farà falta vigilar els permisos del fitxer de la base de
dades per que tant tu com l’usuari `apache` el pogueu modificar.

-   Fes la versió inicial del llibre de visites: una sola pàgina (script
    més plantilla de Genshi) que mostri totes les entrades del llibre de
    visites i al final un formulari per afegir de noves.
-   Fes una versió *paginada* del llibre de visites (mostra 10 entrades
    del llibre per pàgina).
-   Organitza el llibre de visites en diferents pàgines: una per mostrar
    el contingut (paginat); una per afegir una nova entrada. Cal oferir
    una navegació correcta al visitant: tornar a la pàgina inicial;
    cancel·lar l’edició de noves entrades; visitar les entrades
    *paginades*, etc.
-   Ofereix a l’usuari un enllaç o botó per tornar a la pàgina original,
    sigui aquesta quina sigui.


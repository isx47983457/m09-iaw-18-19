#! /usr/bin/python
# -*- coding: utf-8 -*-

import random
import os, sys , session
from genshi.template import TemplateLoader

########################################################################
# Creacio resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(template)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Calcul i definicio variables
########################################################################

llista_colors = ['aqua','black','blue','fuchsia','gray','green','lime','maroon','navy','olive','purple','red','silver','teal','white','yellow']

color = random.sample(llista_colors,1)[0]

plantilla = "background.xml"

write = sys.stdout.write

# HTTP headers

write(session.response_cookie("COLOR",color))

write("Content-Type: text/html\r\n")
write("\r\n")

# received cookies
cookies = session.request_cookies()

# response body

if cookies is None:
    write("<p>First visit or cookies disabled.</p>\n")
else:
	write("<p>Received HTTP header: %s</p>\n" % (os.environ["HTTP_COOKIE"]))
	entorn = {'color':color}	
	pagina(plantilla, entorn)


########################################################################
# Processament peticio
########################################################################

# si cal modificar entorn...
# processar peticio...


sys.exit(0) 

# vim:sw=4:ts=4:ai:et

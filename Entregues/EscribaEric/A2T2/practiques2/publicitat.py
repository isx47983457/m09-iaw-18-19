#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, session, time

from genshi.template import TemplateLoader


########################################################################
# Creació resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)   #instanciem un objecte TemplateLoader

  template = loader.load(template) #carreguem plantilla amb mètode load

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Càlcul i definició variables

########################################################################


cookies = session.request_cookies()
write = sys.stdout.write

write(session.response_cookie("PUBLICIDAD", False))

if cookies is None:
    publi = True
else:
    publi = False


entorn = { 'publicitat' : publi, 'nopublicitat' : not(publi) }

plantilla = "publicitat.xml"

pagina(plantilla, entorn)

sys.exit(0)

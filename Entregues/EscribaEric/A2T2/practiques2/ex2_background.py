#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys , session
from genshi.template import TemplateLoader

########################################################################
# Creacio resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(template)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Calcul i definicio variables
########################################################################


llista_colors = ['yellow','green']

plantilla = "background_2.xml"
write = sys.stdout.write

# HTTP headers
write("Content-Type: text/html\r\n")
write("\r\n")
# received cookies

cookies = session.request_cookies()
# response body

if cookies is None:
	color = llista_colors[0]
	write(session.response_cookie("COLOR",color))
	write("<p>First visit</p>\n")
	#write("<p>Received HTTP header: %s</p>\n" % (os.environ["HTTP_COOKIE"]))
	entorn = {'color': color}
	pagina(plantilla, entorn)
	
    
else:
	write("<p>Received HTTP header: %s</p>\n" % (os.environ["HTTP_COOKIE"]))
	write("<dl\n")
	for (k, v) in cookies.iteritems():
		write("<dt>%s</dt><dd>%s</dd>\n" % (k, v))
		if v == 'green':
			color = llista_colors[0]
		else:
			color = llista_colors[1]
	write("</dl\n")
		
	entorn = {'color':color}
	pagina(plantilla, entorn)


########################################################################
# Processament peticio
########################################################################

# si cal modificar entorn...
# processar peticio...


sys.exit(0) 
# vim:sw=4:ts=4:ai:et
